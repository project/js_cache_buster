<?php

namespace Drupal\js_cache_buster;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Twig extension that adds {{ cache_buster(path) }} function.
 */
class TwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('cache_buster', [$this, 'getCacheBusterPath']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'js_cache_buster';
  }

  /**
   * Return a path with a cache buster string.
   */
  public function getCacheBusterPath($path) {
    $string = $path;
    // Remove leading slash.
    if (strpos($string, '/') === 0) {
      $string = substr($string, 1);
    }
    $buster_key = js_cache_buster_get_key($string);
    return $path . '?v=' . $buster_key;
  }

}
